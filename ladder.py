from lemming import Lemming
class Ladder:
    builder:Lemming
    def __init__(self, x:int, y:int, direction:str, building = 0, built = False):
        def check_coords(coord):
            if (coord < 0) or (coord > 256) or (type(coord) != int):
                raise ValueError('Not a valid coord')
        check_coords(x)
        check_coords(y)
        self.x = x
        self.y = y
        self.direction = direction
        self.built = built
        __right_placeholder = (0, 16, 48, 16, 16)
        __left_placeholder = (0, 16, 64, 16, 16)
        # this attribute measures the progress
        self.building: int = building

    @property
    def image(self):
        if self.direction == 'left':
            if not self.building or self.built:
                return (0,16,16,16,16)
            # if the ladder is in process of construction, then the image should be shown little by little
            elif self.building:

                return(0,32 -(self.building) ,32 - (self.building),self.building, self.building)

        elif self.direction == 'right':
            if not self.building or self.built:
                return (0,16,32,16,16)
            elif self.building:
                return (0,16,48-self.building,self.building, self.building)