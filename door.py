class Door:
    x: int
    y: int
    entrance_or_exit: str # door in is the where the lemmings have to go; door out is where the lemmings come from;
    # door in is where the lemmings have to go
    def __init__(self, x: int, y: int, entrance_or_exit:str ):
        def check_coords(coord):
            if (coord < 0) or (coord > 256) or (type(coord) != int):
                raise ValueError('Not a valid coord')

        check_coords(x)
        check_coords(y)
        self.x = x
        self.y = y
        if entrance_or_exit != 'entrance' and entrance_or_exit != 'exit':
            raise ValueError('Type: entrance or exit')

        if entrance_or_exit == 'entrance':
            self.image = (0, 0, 16, 16, 16)
        if entrance_or_exit == 'exit':
            self.image = (0, 0, 32, 16, 16)



        self.entrance_image = (0, 0, 16, 16, 16)
        self.exit_image = (0,0,32,16,16)
