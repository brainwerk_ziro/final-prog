

class Shovel:
    def __init__(self, x, y, built:bool = False):
        self.x = x
        self.y = y
        self.built = built
    @property
    def image(self):
        if not self.built:
            return(0,48,16,16,16)
