# Here is the matrix of the platforms.
# We first create the matrix with 0s (no block) and 1s (block).
# Conditions:

# The grid is 16x16
# Rows go from 3-15 (start to count from 0)
# Columns go from 0-13
# There will be 7 platforms with random number of blocks from 5-10 (included)
# The rows will be created first


import random
from block import Block


def generate_board():

    rows = []

    # repeat until the 7 rows where the platforms will be drawn are selected
    while len(rows) < 7:
        row = random.randint(3,15)
        # if the row already has a platform, then generate a new row number
        # we can put more conditions in here
        # this reduces the repetition of a row appearing below another one significantly
        if row - 1 in rows:
            continue
        if row not in rows:
            rows.append(row)

    # now the columns

    columns = []
    blocks_list = []
    for i in range(7):
        # number of blocks of the platform
        n_of_blocks = random.randint(5,10)
        blocks_list.append(n_of_blocks)
        # column in which the blocks will be created
        column = random.randint(0,16 - n_of_blocks)
        columns.append(column)



    # now we create the combination matrix
    # first start with the rows

    #first create a matrix filled with 0 with the dimensions of the board
    matrix = []

    for i in range(16):
        column = []
        for a in range(16):
            column.append(0)
        matrix.append(column)

    # now fill if with 1 where there is a block
    total_blocks = 0
    rows_pos = 0
    for i in range(7):
        for a in range(3,16):
            if rows[rows_pos] == a:
                col_pos = rows_pos
                b = columns[col_pos]
                for c in range(blocks_list[rows_pos]):
                    matrix[a][b] = 1
                    total_blocks += 1
                    b += 1

        rows_pos += 1

    blocks_x = []  # a list with the x positions of the blocks
    blocks_y = []  # a list with the y positions of the blocks
    blocks_objects = []  # a list with the blocks objects
    for i in range(16):
        for a in range(16):
            if matrix[i][a] == 1:
                blocks_x.append(a)  # the x position is the position of the column
                blocks_y.append(i)  # the y position is the position of the row

    for i in range(len(blocks_x)):
        # we set the coordinates of the objects
        x = blocks_x[i]
        y = blocks_y[i]

        # the probability that one out block is destructible is 0.33
        destructible = not random.randint(0, 2)
        # we need to make sure that the last platform is completely destructible to avoid stale game over
        # so if
        if y == 15:
            destructible = True

        # if the value is different than 0, returns True
        destructible = bool(destructible)

        # we initialize the object with those coordinates
        block = Block(x, y, destructible)
        # we add the object to the list of objects
        blocks_objects.append(block)


    # we now create the doors

    y1 = random.randint(0, len(blocks_y) - 1)
    entrance_y = blocks_y[y1] - 1
    # the x value must coincide with the y value. We have to take into account that if two rows are placed one below
    # the other, then that would not be a valid position for the door
    while entrance_y in blocks_y:
        y1 = random.randint(0, len(blocks_x) - 1)
        entrance_y = blocks_y[y1] - 1
    # the y position of the entrance
    entrance_x = blocks_x[y1]

    # do the same for the exit
    y2 = random.randint(0, len(blocks_y) - 1)
    exit_y = blocks_y[y2] - 1
    while exit_y in blocks_y or exit_y == entrance_y:
        y2 = random.randint(0, len(blocks_y) - 1)
        exit_y = blocks_y[y2] - 1
    exit_x = blocks_x[y2]

    # and we now put a 2 wherever there's a door in the board matrix
    matrix[entrance_y][entrance_x] = 2.1
    matrix[exit_y][exit_x] = 2.2

    # now we stablish the coordinates of the entrance and exit
    entrance = []
    exit = []
    for i in range(16):
        for a in range(16):
            if matrix[i][a] == 2.1:
                entrance.append(a)  # the x position is the position of the column
                entrance.append(i)
            if matrix[i][a] == 2.2:
                exit.append(a)  # the x position is the position of the column
                exit.append(i)
    return blocks_objects, entrance, exit