from random import randint
from typing import List, Any
from block import Block
from blocker import Blocker
from ladder import Ladder
from lemming import Lemming
import pyxel
from lemming import Lemming
from generate_board import generate_board
from shovel import Shovel
from umbrella import Umbrella


class Board:


    def newlevel(self): # this creates a new level, call this when game starts(in __init__) and when we change levels
        if self.gameover:
            self.level=1
        else:
            self.level+=1
        self.blocks_objects, self.entrance, self.exit = generate_board()

        self.ladders = []
        self.umbrellas = []
        self.blockers = []
        self.shovels = []

        self.lemmings_number=randint(10,20)
        self.ladders_number=randint(5,10)
        self.umbrella_number=randint(1,3)
        self.shovels_number=randint(3,5)
        self.blockers_number=randint(3,5)
        if randint(0, 1) == 0:
            self.init_dir = "R"
        else:
            self.init_dir = "L"
        self.lems = []
        self.blems = []
        self.shlems = []

        self.lems_cntr = 0
        self.lems_alive = 0
        self.lems_dead = 0
        self.lems_saved = 0

        self.selection_block = Block(self.entrance[0] * 16,
                                     self.entrance[1] * 16)

        self.gameover=False
        self.pause=True
        self.started=False

    def __init__(self):
        self.board_width = 256
        self.board_height = 256
        self.caption: str = 'CRAZY SNAILS'
        self.level=0
        self.background_color = 0

        self.entrance_image = (0, 0, 16, 16, 16)
        self.exit_image = (0, 0, 32, 16, 16)
        self.white = 7
        self.black = 0
        self.blue = 5
        self.green = 11
        self.pink = 8
        self.gameover=False
        self.menu=True
        pyxel.init(self.board_width, self.board_height, caption=self.caption, fps=30)
        pyxel.image(0).load(0, 0, "mainmenu.png")
        pyxel.run(self.update, self.draw)

    def update(self):
        if self.menu:
            if pyxel.btnp(pyxel.KEY_ENTER):
                self.menu=False
                pyxel.load("lemming.pyxres")
                self.newlevel()
        else:
            if pyxel.btnp(pyxel.KEY_ENTER):
                self.newlevel()

            # To update each time the keys are pressed we need to:
            # update the coordinates of the selection block each time depending on which key is pressed
            # draw the selection block in the draw function with the new coordinates

            # Viktor - changed the method to btnp so it's only a single keystroke
            # Before it was way too sensitive and easily skipped cells
            if not self.gameover:
                if pyxel.btnp(pyxel.KEY_RIGHT):
                    if self.selection_block.x != 256 - 16:
                        self.selection_block.x += 16
                if pyxel.btnp(pyxel.KEY_LEFT):
                    if self.selection_block.x != 0:
                        self.selection_block.x -= 16
                if pyxel.btnp(pyxel.KEY_UP):
                    if self.selection_block.y != 32:
                        self.selection_block.y -= 16
                if pyxel.btnp(pyxel.KEY_DOWN):
                    if self.selection_block.y != 256 - 16:
                        self.selection_block.y += 16

                # this is to store the position where the new tools are being built
                # to build the right-upwards ladders
                if len(self.ladders) < self.ladders_number:
                    if pyxel.btnp(pyxel.KEY_1):

                        can_build_ladder = True  # this will be a bool that will go through the conditions to see if a ladder
                        # can be placed there
                        # go through all the ladders
                        for ladder in self.ladders:
                            # if a ladder has already been built there
                            if ladder.x == self.selection_block.x and ladder.y == self.selection_block.y and ladder.built:
                                # if ladder.direction == 'right':
                                # this means that a ladder in that direction has already been constructed
                                # can_build_ladder = False
                                # if the direction is upwards, then the ladder will be replaced
                                # elif ladder.direction == 'left':
                                #     ladder.direction = 'right'
                                can_build_ladder = False
                            else:
                                # if there is a ladder that will be constructed in the same direction
                                if ladder.x == self.selection_block.x and ladder.y == self.selection_block.y:
                                    if ladder.direction == 'right':
                                        # this means the user wants to remove the ladder
                                        self.ladders.remove(ladder)
                                        # nothing will be built there
                                        can_build_ladder = False
                                    # if the ladder in construction goes to the left
                                    elif ladder.direction == 'left':
                                        # change the direction of the unbuilt ladder
                                        ladder.direction = 'right'
                                        # we will change the direction instead of building a new one
                                        can_build_ladder = False
                        # this creates a ladder in the place of an umbrella and
                        for umbrella in self.umbrellas:
                            if umbrella.x == self.selection_block.x and umbrella.y == self.selection_block.y:
                                self.umbrellas.remove(umbrella)  # the ladder will replace the umbrella
                        # this checks if there is a block in that position:
                        for block in self.blocks_objects:
                            # if there is a block
                            if self.selection_block.x == block.x * 16 and self.selection_block.y == block.y * 16:
                                can_build_ladder = False
                        if can_build_ladder:
                            # if there is nothing in that cell, just create the ladder and append it
                            ladder = Ladder(self.selection_block.x, self.selection_block.y, 'right')
                            self.ladders.append(ladder)

                # to build the left-upwards ladders
                if len(self.ladders) < self.ladders_number:
                    # note that we do not loop over the umbrellas because a ladder can be constructed on top
                    if pyxel.btnp(pyxel.KEY_2):
                        can_build_ladder = True  # this will be a bool that will go through the conditions to see if a ladder
                        # can be placed there
                        # go through all the ladders
                        for ladder in self.ladders:
                            # if a ladder has already been built there

                            if ladder.x == self.selection_block.x and ladder.y == self.selection_block.y:
                                if ladder.built:
                                    # if ladder.direction == 'left':
                                    # this means that a ladder in that direction has already been constructed
                                    # can_build_ladder = False
                                    # if the direction is upwards, then the ladder will be replaced
                                    # elif ladder.direction == 'right':
                                    #     ladder.direction = 'left'
                                    can_build_ladder = False
                                # in the case that the ladder has not yet been built
                                else:
                                    # if there is a ladder that will be constructed in the same direction
                                    if ladder.direction == 'left':
                                        # this means the user wants to remove the ladder
                                        self.ladders.remove(ladder)
                                        # nothing will be built there
                                        can_build_ladder = False
                                    # if the ladder in construction goes to the right
                                    elif ladder.direction == 'right':
                                        # change the direction of the unbuilt ladder
                                        ladder.direction = 'left'

                                        can_build_ladder = False

                        # this checks if there is a block in that position:
                        for block in self.blocks_objects:
                            # if there is a block
                            if self.selection_block.x == block.x * 16 and self.selection_block.y == block.y * 16:
                                can_build_ladder = False
                        # check if there is a blocker at that position
                        for umbrella in self.umbrellas:
                            if umbrella.x == self.selection_block.x and umbrella.y == self.selection_block.y:
                                self.umbrellas.remove(umbrella)  # the ladder will replace the umbrella
                        # check if there is a blocker at that position
                        for blocker in self.blockers:
                            if self.selection_block.x == blocker.x and self.selection_block.y == blocker.y:
                                if blocker.built:
                                    can_build_ladder = False
                                else:
                                    self.blockers.remove(blocker)

                        if can_build_ladder:
                            # if there is nothing in that cell, just create the ladder and append it
                            ladder = Ladder(self.selection_block.x, self.selection_block.y, 'left')
                            self.ladders.append(ladder)

                # Key 3 will build the umbrella
                if len(self.umbrellas) < self.umbrella_number:
                    if pyxel.btnp(pyxel.KEY_3):
                        can_build_there = True  # this will be a bool that will go through the conditions to see if a tool
                        # can be placed there
                        # go through all the umbrellas
                        for umbrella in self.umbrellas:
                            # if an umbrella has already been built there then it means that the player wants to delete it
                            if umbrella.x == self.selection_block.x and umbrella.y == self.selection_block.y:
                                if not umbrella.built:  # if the umbrella has not yet been built
                                    self.umbrellas.remove(umbrella)
                                    can_build_there = False

                        # this checks if there is a block in that position:
                        for block in self.blocks_objects:
                            # if there is a block
                            if self.selection_block.x == block.x * 16 and self.selection_block.y == block.y * 16:
                                can_build_there = False
                        # if there is a ladder in that position, then we cannot construct there, as they are irremovable
                        for ladder in self.ladders:
                            # this allows changing the ladder for an umbrella before it has been constructed
                            if ladder.x == self.selection_block.x and ladder.y == self.selection_block.y:
                                if ladder.built:
                                    can_build_there = False
                                else:
                                    self.ladders.remove(ladder)
                        # if there is an umbrella, do not build another one
                        for umbrella in self.umbrellas:
                            if umbrella.x == self.selection_block.x and umbrella.y == self.selection_block.y and umbrella.built:
                                can_build_there = False
                        # this checks if there is a blocker in that position
                        for blocker in self.blockers:
                            if self.selection_block.x == blocker.x and self.selection_block.y == blocker.y:
                                if blocker.built:
                                    can_build_there = False
                                else:
                                    self.blockers.remove(blocker)

                        if can_build_there:
                            # if there is nothing in that cell, just create the blocker and append it
                            umbrella = Umbrella(self.selection_block.x, self.selection_block.y)
                            self.umbrellas.append(umbrella)

                # Key 4 will build a blocker
                if len(self.blockers) < self.blockers_number:
                    if pyxel.btnp(pyxel.KEY_4):
                        can_build_there = True  # this will be a bool that will go through the conditions to see if a tool
                        # can be placed there
                        # go through all the umbrellas
                        for blem in self.blems:
                            if blem.x // 16 == self.selection_block.x // 16 and blem.y // 16 == self.selection_block.y // 16:
                                blem.mov = blem.prev_dir
                                self.blems.remove(blem)
                                can_build_there = False
                        for blocker in self.blockers:
                            # if an umbrella has already been built there then it means that the player wants to delete it
                            if blocker.x == self.selection_block.x and blocker.y == self.selection_block.y:
                                if not blocker.built:  # if the blocker has not yet been built
                                    self.blockers.remove(blocker)
                                    can_build_there = False

                        # if there is an blocker, do not build another one
                        for blocker in self.blockers:
                            if blocker.x == self.selection_block.x and blocker.y == self.selection_block.y and blocker.built:
                                can_build_there = False
                        # this checks if there is a block in that position:
                        for block in self.blocks_objects:
                            # if there is a block
                            if self.selection_block.x == block.x * 16 and self.selection_block.y == block.y * 16:
                                can_build_there = False
                        # if there is a ladder in that position, then we cannot construct there, as they are irremovable
                        for ladder in self.ladders:
                            # this allows changing the ladder for a blocker before it has been constructed
                            if ladder.x == self.selection_block.x and ladder.y == self.selection_block.y:
                                # Viktor - changed this as we should be able to make a blocker on a built ladder
                                if not ladder.built:
                                    self.ladders.remove(ladder)
                        # this checks if there is an umbrella in that position
                        for umbrella in self.umbrellas:
                            if self.selection_block.x == umbrella.x and self.selection_block.y == umbrella.y:
                                if umbrella.built:
                                    can_build_there = False
                                else:
                                    self.umbrellas.remove(umbrella)

                        if can_build_there:
                            # if there is nothing in that cell, just create the blocker and append it
                            blocker = Blocker(self.selection_block.x, self.selection_block.y)
                            self.blockers.append(blocker)


                # Key 5 is the shovel
                if len(self.shovels) < self.shovels_number:
                    if pyxel.btnp(pyxel.KEY_5):
                        can_build_there = False # default
                        for block in self.blocks_objects:
                            # if not a destructible block is selected
                            if self.selection_block.x == block.x * 16 and self.selection_block.y == block.y * 16 and \
                                    block.destructible:
                                # if a shovel is selected if the shovel has not been constructed yet


                                can_build_there = True
                                for shovel in self.shovels:
                                    if shovel.x == self.selection_block.x and shovel.y == self.selection_block.y and not \
                                            shovel.built:
                                        self.shovels.remove(shovel)
                                        can_build_there = False



                        if can_build_there:
                            shovel = Shovel(self.selection_block.x, self.selection_block.y)
                            self.shovels.append(shovel)
                        #print(self.shovels)
                # once the shovel is built, the block disappears
                for shovel in self.shovels:
                    if shovel.built:
                        for block in self.blocks_objects:
                            if shovel.x == block.x*16 and shovel.y == block.y*16:
                                self.blocks_objects.remove(block)



                # pause
                if pyxel.btnp(pyxel.KEY_SPACE):
                    if self.pause:
                        self.pause = False
                        self.started=True
                    else:
                        self.pause = True



                if not self.pause:

                    # spawning lemmings
                    # will spawn one every second until we got the count
                    # we use a counter instead of lem(lems) because if lemmings start escaping before they are done spawning,
                    # they will just spawn indefinitely
                    if self.lems_cntr < self.lemmings_number and not pyxel.frame_count % 30:
                        self.lems.append(Lemming(self.entrance[0] * 16 + 4, self.entrance[1] * 16 + 8, self.init_dir))
                        self.lems_cntr += 1
                        self.lems_alive += 1

                    # collision detection for lemmings... oh boy
                    # the old one got complicated real quick so I decided to redo it
                    # the old one was better optimized, so might have to partially revert to it
                    # it's stored in collision.old.py
                    # this one will have more reduntant checks(=slower), but at least the code will be more concise
                    # and each type of collision will be in a separate block
                    # so no more scrolling around looking for whatever the fuck I'm trying to change

                    # just so you know, we have conditions like if lemming.x % 16 == 8 for optimization
                    # because the level is divided into 14x16 cells and therefore we can only meet an obstacle on the edge of a cell
                    # so checking for an obstacle every pixel would be just stupid
                    # lemmings are stupid, though...
                    # well, probably not this stupid

                    for lemming in self.lems:

                        ### EDGE OF THE SCREEN CHECKS ###

                        # lemming will turn around in the same manner when he's on the edge of the screen
                        # I know this looks a bit messy, I tried to make a method for this but for some reason I can't get it to work
                        if lemming.mov == "R" and lemming.x == 248:
                            lemming.mov = "L"
                        if lemming.mov == "L" and lemming.x == 0:
                            lemming.mov = "R"
                        if lemming.mov == "AscR" and lemming.x == 248:
                            lemming.mov = "DescL"
                        if lemming.mov == "AscL" and lemming.x == 0:
                            lemming.mov = "DescR"
                        if lemming.mov == "DescL" and lemming.x == 0:
                            lemming.mov = "AscR"
                        if lemming.mov == "DescR" and lemming.x == 248:
                            lemming.mov = "AscL"
                        if lemming.mov == "AscR" and lemming.y == 32:
                            lemming.mov = "DescL"
                        if lemming.mov == "AscL" and lemming.y == 32:
                            lemming.mov = "DescR"

                        ### EDGE OF THE SCREEN CHECKS END ###

                        # shovel interaction
                        #shovel below
                        if lemming.mov in ("L", "R") and lemming.x % 16 == 4:
                            for i in self.shovels:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1:
                                    for k in self.blocks_objects:
                                        if i.x//16==k.x and i.y//16==k.y:
                                            for l in self.lems:
                                                if l.x//16==k.x and l.y//16-1==k.y:
                                                    l.mov="Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h=lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)
                        #shovel above
                        if lemming.mov in ("AscR", "AscL") and lemming.y % 16 == 0:
                            for i in self.shovels:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 - 1:
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)

                        #shovel vertical
                        if lemming.mov == "R" and lemming.x % 16 == 8:
                            for i in self.shovels:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16  == lemming.y // 16:
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)
                        if lemming.mov == "L" and lemming.x % 16 == 0:
                            for i in self.shovels:
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16:
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)
                        if lemming.mov == "AscR" and lemming.y % 16 == 8:
                            for i in self.shovels:
                                if i.x //16 == lemming.x // 16 + 1 and i.y //16 == lemming.y // 16:
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)

                        if lemming.mov == "AscL" and lemming.y % 16 == 8:
                            for i in self.shovels:
                                if i.x //16 == lemming.x // 16 - 1 and i.y //16 == lemming.y // 16:
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)

                        if lemming.mov == "DescR" and lemming.x % 16 == 8:
                            for i in self.shovels:
                                if i.x //16 == lemming.x // 16 + 1 and i.y //16 == lemming.y // 16 :
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)

                        if lemming.mov == "DescL" and lemming.x % 16 == 0:
                            for i in self.shovels:
                                if i.x //16 == lemming.x // 16 - 1 and i.y //16 == lemming.y // 16 :
                                    for k in self.blocks_objects:
                                        if i.x // 16 == k.x and i.y // 16 == k.y:
                                            for l in self.lems:
                                                if l.x // 16 == k.x and l.y // 16 - 1 == k.y:
                                                    l.mov = "Fal"
                                            self.blocks_objects.remove(k)
                                    lemming.prev_h = lemming.y
                                    lemming.mov = "Fal"
                                    self.shovels.remove(i)

                        ### HORIZONTAL MOVEMENT CHECKS ###

                        # when lemming is moving horizontally and is on the edge of the cell, this will check if there's an obstacle
                        # if there is, he turns around
                        # ladder ascension also goes here
                        if lemming.mov == "R" and lemming.x % 16 == 8:
                            for i in self.blocks_objects:
                                if i.x == (lemming.x + 8) // 16 and i.y == lemming.y // 16:
                                    lemming.mov = "L"

                        if lemming.mov == "L" and lemming.x % 16 == 0:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 - 1 and i.y == lemming.y // 16:
                                    lemming.mov = "R"

                        # if lemming goes horizontally and hits a ladder, he starts ascending it
                        if lemming.mov == "R" and lemming.x % 16 == 8:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right":
                                    if i.built:
                                        lemming.mov = "AscR"

                        if lemming.mov == "L" and lemming.x % 16 == 0:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16 and i.direction == "left":
                                    if i.built:
                                        lemming.mov = "AscL"

                        # if lemming goes horizontally and there's ladder below him, he starts descending it
                        # we check for vertical obstacles earlier this frame so I don't think we have to account for that
                        # but this should be the first place to inspect in case of ladder-related bugs
                        if (lemming.mov == "R" and lemming.x % 16 == 0):
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "left":
                                    if i.built:
                                        lemming.mov = "DescR"

                        if (lemming.mov == "L" and lemming.x % 16 == 8):
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "right":
                                    if i.built:
                                        lemming.mov = "DescL"

                        ### HORIZONTAL MOVEMENT CHECKS END ###

                        ### DIAGONAL MOVEMENT CHECKS ###

                        # when a lemming gets on top(or bottom) of the ladder, great many things can happen
                        # or even before that...

                        # if a lemming goes up and bonks his head on the ceiling, he will turn around and go down
                        # as my mom says, "If you had a brain you'd have a concussion"
                        if lemming.mov == "AscR" and lemming.y % 16 == 0:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 and i.y == lemming.y // 16 - 1:
                                    lemming.mov = "DescL"
                        if lemming.mov == "AscL" and lemming.y % 16 == 0:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 and i.y == lemming.y // 16 - 1:
                                    lemming.mov = "DescR"

                        # if a lemming meets a vertical obstacle, he will turn around as well
                        if lemming.mov == "AscR" and lemming.x % 16 == 8:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 + 1 and i.y == lemming.y // 16:
                                    lemming.mov = "DescL"
                        if lemming.mov == "AscL" and lemming.x % 16 == 0:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 - 1 and i.y == lemming.y // 16:
                                    lemming.mov = "DescR"
                        if lemming.mov == "DescL" and lemming.x % 16 == 0:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 - 1 and i.y == lemming.y // 16:
                                    lemming.mov = "AscR"
                        if lemming.mov == "DescR" and lemming.x % 16 == 8:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 + 1 and i.y == lemming.y // 16:
                                    lemming.mov = "AscL"

                        # when lemming gets on top of the ladder without bonking his head or breaking his nose
                        # he can be finally allowed to move horizontally and escape this nightmare realm of diagonal floating
                        if lemming.mov == "AscR" and lemming.y % 16 == 8:
                            lemming.mov = "R"
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right":
                                    if i.built:
                                        lemming.mov = "AscR"
                                    elif i.building>0:
                                        lemming.mov = "DescL"
                        if lemming.mov == "AscL" and lemming.y % 16 == 8:
                            lemming.mov = "L"
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16 and i.direction == "left":
                                    if i.built:
                                        lemming.mov = "AscL"
                                    elif i.building>0:
                                        lemming.mov = "DescR"

                        if lemming.mov == "DescL" and lemming.y % 16 == 8:
                            lemming.mov = "L"
                        if lemming.mov == "DescR" and lemming.y % 16 == 8:
                            lemming.mov = "R"

                        # we had this check earlier, but whatever
                        # we need to perform it again in some cases
                        if (lemming.mov == "R" and lemming.x % 16 == 0):
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "left":
                                    if i.built:
                                        lemming.mov = "DescR"
                        if (lemming.mov == "L" and lemming.x % 16 == 8):
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "right":
                                    if i.built:
                                        lemming.mov = "DescL"

                        # if we have ladders in V-shape
                        # the lemming will go on another one
                        if lemming.mov == "DescL" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 and i.direction == "left":
                                    if i.built:
                                        lemming.mov = "AscL"
                        if lemming.mov == "DescR" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right":
                                    if i.built:
                                        lemming.mov = "AscR"

                        ### DIAGONAL MOVEMENT CHECKS END ###

                        ### VERTICAL MOVEMENT CHECKS ###

                        # if there's no ground below the dude, he'll fall to his death(rip)
                        # we assume the dude's falling and then look for the ground below him
                        # this should be at the bottom so a dude falls only after we check for any kind of collision
                        if (lemming.mov == "R" and lemming.x % 16 == 0) or (lemming.mov == "L" and lemming.x % 16 == 8):
                            falling = True
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 and i.y == lemming.y // 16 + 1:
                                    falling = False
                            if falling:
                                lemming.prev_dir = lemming.mov
                                lemming.prev_h = lemming.y  # once the lemming starts falling, we store the height he's been at
                                lemming.mov = "Fal"

                        # if the lemming is falling and there's ground below him, he stops
                        if lemming.mov == "Fal" and lemming.y % 16 == 8:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 and i.y == (lemming.y + 8) // 16:
                                    if lemming.y > lemming.prev_h + 32:  # if the drop height is more than 2 blocks, the lemming dies
                                        lemming.mov = "Ded"
                                        self.lems_dead += 1
                                        self.lems_alive -= 1
                                    else:
                                        lemming.mov = lemming.prev_dir

                        # same but for umbrellas
                        if lemming.mov == "Umb" and lemming.y % 16 == 4:
                            for i in self.blocks_objects:
                                if i.x == lemming.x // 16 and i.y == lemming.y // 16 + 1:
                                    lemming.y += 4
                                    lemming.mov = lemming.prev_dir

                        # collision with level floor
                        if lemming.y == 248 and lemming.mov == "Fal":
                            if lemming.y > lemming.prev_h + 32:  # if the drop height is more than 2 blocks, the lemming dies
                                lemming.mov = "Ded"
                                self.lems_dead += 1
                                self.lems_alive -= 1
                            else:
                                lemming.mov = lemming.prev_dir

                        if lemming.y == 244 and lemming.mov == "Umb":
                            lemming.y = 248
                            lemming.mov = lemming.prev_dir

                        # collision with ladders
                        # we have to check this every 8 pixels, not every 16
                        # will have to check this even more often if we implement falling from halfway built ladders
                        if lemming.mov == "Fal" and lemming.y % 8 == 0:
                            for i in self.ladders:
                                if i.built:
                                    if lemming.y % 16 == 8:
                                        if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1:
                                            if (i.direction == "right" and lemming.prev_dir == "L") or (
                                                    i.direction == "left" and lemming.prev_dir == "R"):
                                                if lemming.y > lemming.prev_h + 32 and lemming.mov == "Fal":  # if the drop height is more than 2 blocks, the lemming dies
                                                    lemming.mov = "Ded"
                                                    self.lems_dead += 1
                                                    self.lems_alive -= 1
                                                else:
                                                    if lemming.prev_dir == "L":
                                                        lemming.mov = "DescL"
                                                    else:

                                                        lemming.mov = "DescR"
                                    if lemming.y % 16 == 0:
                                        if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16:
                                            if (i.direction == "right" and lemming.prev_dir == "R") or (
                                                    i.direction == "left" and lemming.prev_dir == "L"):
                                                if lemming.y > lemming.prev_h + 32:  # if the drop height is more than 2 blocks, the lemming dies
                                                    lemming.mov = "Ded"
                                                    self.lems_dead += 1
                                                    self.lems_alive -= 1
                                                else:
                                                    if lemming.prev_dir == "L":
                                                        if i.built:
                                                            lemming.mov = "AscL"
                                                    else:
                                                        if i.built:
                                                            lemming.mov = "AscR"

                        if lemming.mov == "Umb" and lemming.y % 8 == 4:
                            for i in self.ladders:
                                if i.built:
                                    if lemming.y % 16 == 4:
                                        if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1:
                                            if (i.direction == "right" and lemming.prev_dir == "L") or (
                                                    i.direction == "left" and lemming.prev_dir == "R"):
                                                lemming.y += 4
                                                if lemming.prev_dir == "L":
                                                    lemming.mov = "DescL"
                                                else:

                                                    lemming.mov = "DescR"
                                    if lemming.y % 16 == 12:
                                        if i.x // 16 == lemming.x // 16 and i.y // 16 == (lemming.y + 12) // 16:
                                            if (i.direction == "right" and lemming.prev_dir == "R") or (
                                                    i.direction == "left" and lemming.prev_dir == "L"):
                                                lemming.y += 4
                                                if lemming.prev_dir == "L":
                                                    if i.built:
                                                        lemming.mov = "AscL"
                                                else:
                                                    if i.built:
                                                        lemming.mov = "AscR"

                        if lemming.mov == "Fal" and lemming.y % 16 == 8:
                            for i in self.umbrellas:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == (lemming.y + 8) // 16:
                                    lemming.mov = 'Umb'
                                    i.built = True

                        ### VERTICAL MOVEMENT CHECKS END ###

                        ### DOOR TOOL ETC CHECKS ###

                        # door collision
                        # in theory this should despawn the lemming that touches the exit door
                        if lemming.mov in ("R", "L") and lemming.x // 16 == self.exit[0] and lemming.y // 16 == self.exit[
                            1] and lemming.x % 16 == 4:
                            self.lems.remove(lemming)
                            self.lems_saved += 1
                            self.lems_alive -= 1

                        # unbuilt ladder collision
                        if lemming.mov == "R" and lemming.x % 16 == 8:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "R"
                                        i.builder = lemming
                                        i.building = 1
                                        i.y += 15
                                    else:
                                        lemming.mov = "L"

                        if lemming.mov == "L" and lemming.x % 16 == 0:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16 and i.direction == "left" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "L"
                                        i.builder = lemming
                                        i.building = 1
                                        i.x += 15
                                        i.y += 15
                                    else:
                                        lemming.mov = "R"

                        if (lemming.mov == "R" and lemming.x % 16 == 8):
                            descend = False
                            for i in self.ladders:
                                if i.x//16==lemming.x//16 and i.y//16==lemming.y//16+1 and i.direction=="right":
                                    descend=True
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "left" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "DescR"
                                        i.builder = lemming
                                        i.building = 1
                                        i.x += 15
                                        i.y += 15
                                    elif descend:
                                        lemming.mov = "DescL"
                                    else:
                                        lemming.mov = "L"

                        if (lemming.mov == "L" and lemming.x % 16 == 0):
                            descend = False
                            for i in self.ladders:
                                if i.x//16==lemming.x//16 and i.y//16==lemming.y//16+1 and i.direction=="left":
                                    descend=True
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "right" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "DescL"
                                        i.builder = lemming
                                        i.building = 1
                                        i.y += 15
                                    elif descend:
                                        lemming.mov = "DescR"
                                    else:
                                        lemming.mov = "R"

                        if lemming.mov == "AscR" and lemming.x % 16 == 8:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "AscR"
                                        i.builder = lemming
                                        i.building = 1
                                        i.x += 15
                                        i.y += 15

                        if lemming.mov == "AscL" and lemming.x % 16 == 0:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 - 1 and i.y // 16 == lemming.y // 16 and i.direction == "left" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "AscL"
                                        i.builder = lemming
                                        i.building = 1
                                        i.x += 15
                                        i.y += 15

                        if lemming.mov == "DescL" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "right" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "DescL"  # have to do this here
                                        i.builder = lemming
                                        i.building = 1
                                        i.y += 15
                                    else:
                                        lemming.mov = "AscR"
                        if lemming.mov == "DescR" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 + 1 and i.direction == "left" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "DescR"  # have to do this here
                                        i.builder = lemming
                                        i.building = 1
                                        i.x += 15
                                        i.y += 15
                                    else:
                                        lemming.mov = "AscL"
                        # V-shaped ladder building
                        # marking this specifically since it's especially prone to problems
                        if lemming.mov == "DescL" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 and i.direction == "left" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "AscL"
                                        i.builder = lemming
                                        i.building = 1
                                        i.y += 15
                                        i.x += 15
                                    else:
                                        lemming.mov = "AscR"
                        if lemming.mov == "DescR" and lemming.x % 16 == 12:
                            for i in self.ladders:
                                if i.x // 16 == lemming.x // 16 + 1 and i.y // 16 == lemming.y // 16 and i.direction == "right" and not i.built:
                                    if i.building == 0:
                                        lemming.mov = "Builder"
                                        lemming.prev_dir = "AscR"
                                        i.builder = lemming
                                        i.building = 1
                                        i.y += 15
                                    else:
                                        lemming.mov = "AscL"

                        # blocker interaction
                        # creating a blocker lemming
                        if lemming.x % 16 == 4:
                            for i in self.blockers:
                                if lemming.mov in ("R", "L") and i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16:
                                    self.blockers.remove(i)
                                    lemming.mov = "Blocker"
                                    self.blems.append(lemming)
                                if lemming.mov in ("AscL", "AscR", "DescL",
                                                   "DescR") and i.x // 16 == lemming.x // 16 and i.y // 16 == lemming.y // 16 + 1:
                                    self.blockers.remove(i)
                                    lemming.mov = "Blocker"
                                    self.blems.append(lemming)
                        # interacting with a blocker lemming
                        if lemming.x % 16 == 12 and lemming.mov in ("R", "AscR", "DescR"):
                            for i in self.blems:
                                if i.x // 16 == lemming.x // 16 + 1 and (
                                        (i.y // 16 == lemming.y // 16 and lemming.mov in ("R", "DescR")) or (
                                        i.y // 16 == lemming.y // 16 - 1 and lemming.mov == "AscR")):
                                    if lemming.mov == "R":
                                        lemming.mov = "L"
                                    if lemming.mov == "AscR":
                                        lemming.mov = "DescL"
                                    if lemming.mov == "DescR":
                                        lemming.mov = "AscL"
                        if lemming.x % 16 == 12 and lemming.mov in ("L", "AscL", "DescL"):
                            for i in self.blems:
                                if i.x // 16 == lemming.x // 16 and (
                                        (i.y // 16 == lemming.y // 16 and lemming.mov in ("L", "DescL")) or (
                                        i.y // 16 == lemming.y // 16 - 1 and lemming.mov == "AscL")):
                                    if lemming.mov == "L":
                                        lemming.mov = "R"
                                    if lemming.mov == "AscL":
                                        lemming.mov = "DescR"
                                    if lemming.mov == "DescL":
                                        lemming.mov = "AscR"


                        ### DOOR TOOL ETC CHECKS END ###

                        # lemming movement
                        if lemming.mov == "R":
                            lemming.x += 1
                        if lemming.mov == "L":
                            lemming.x -= 1
                        if lemming.mov == "Fal":
                            lemming.y += 4  # this has to be a divider of 16, or the lemmings will get stuck in terrain after falling
                        if lemming.mov == "Umb":
                            lemming.y += 1  # with umbrella the fall is slower
                        if lemming.mov == "AscR":  # ascending the ladder to the right
                            lemming.x += 1
                            lemming.y -= 1
                        if lemming.mov == "AscL":
                            lemming.x -= 1
                            lemming.y -= 1
                        if lemming.mov == "DescR":  # descending the ladder to the left
                            lemming.x += 1
                            lemming.y += 1
                        if lemming.mov == "DescL":
                            lemming.x -= 1
                            lemming.y += 1
                        # there's also states "blocker" and "Ded" which don't cause movement

                        # store for the next loop
                        lemming.prev_building = lemming.building

                    # this increments the progress in building the ladders
                    for ladder in self.ladders:
                        if ladder.building:
                            # if the ladder has progress 16 (16x16 pixels)
                            if ladder.building == 16:
                                # the ladder is completely built, lemmings can now interact with it
                                ladder.built = True
                                # the ladder is not being built anymore
                                ladder.building = 0
                                ladder.builder.mov = ladder.builder.prev_dir
                            else:

                                # small increments
                                if pyxel.frame_count % 4 == 0:
                                    if ladder.direction == 'left':
                                        ladder.building += 1
                                        ladder.y -= 1
                                        ladder.x -= 1
                                    elif ladder.direction == 'right':
                                        ladder.building += 1
                                        ladder.y -= 1

                    #switch level conditions
                    if self.lemmings_number == self.lems_dead:
                        self.gameover = True
                    elif self.lems_cntr==self.lemmings_number and self.lems_alive==0:
                        self.newlevel()

    def draw(self):
        if self.menu:
            pyxel.blt(0, 0, 0, 0, 0, 256, 256)
            for i in range(1, 56, 7):
                for k in range(1, 255, 58):
                    pyxel.text(k, i, "PRESS [ENTER]", pyxel.frame_count%16)
            for i in range(174, 255, 7):
                for k in range(1, 255, 58):
                    pyxel.text(k, i, "PRESS [ENTER]", pyxel.frame_count%16)
        else:
            pyxel.cls(self.background_color)  # this is the background color

            if self.gameover:
                for i in range(1, 255, 7):
                    for k in range(1, 255, 40):
                        pyxel.text(k, i, "GAME OVER", 8)
                pyxel.rect(74,120,110,11,self.black)
                pyxel.text(78,122,"Press [ENTER] to try again",self.white)
            else:
                pyxel.blt(self.entrance[0] * 16, self.entrance[1] * 16, *self.entrance_image, colkey=self.black)
                pyxel.blt(self.exit[0] * 16, self.exit[1] * 16, *self.exit_image, colkey=self.black)

                # draw the blocks
                for block in self.blocks_objects:
                    pyxel.blt(block.x * 16, block.y * 16, *block.image)

                # this draws the ladders:
                for ladder in self.ladders:
                    # if the ladder has not yet been built by the lemmings, it will be intermittent
                    if not ladder.built and not ladder.building:
                        if 0 < pyxel.frame_count % 20 < 10:
                            pyxel.blt(ladder.x, ladder.y, *ladder.image)
                    elif ladder.building:
                        pyxel.blt(ladder.x, ladder.y, *ladder.image)
                    elif ladder.built:
                        pyxel.blt(ladder.x, ladder.y, *ladder.image)
                        # build a fixed ladder

                for umbrella in self.umbrellas:
                    if not umbrella.built:
                        if 0 < pyxel.frame_count % 20 < 10:
                            # intermittent umbrella
                            pyxel.blt(umbrella.x, umbrella.y, *umbrella.image, colkey=self.black)
                    else:
                        # fixed umbrella
                        pyxel.blt(umbrella.x, umbrella.y, *umbrella.image)
                for blocker in self.blockers:
                    if not blocker.built:
                        # intermittent blocker
                        if 0 < pyxel.frame_count % 20 < 10:
                            pyxel.blt(blocker.x, blocker.y, *blocker.image, colkey=self.black)
                    else:
                        # fixed blocker
                        pyxel.blt(blocker.x, blocker.y, *blocker.image)

                # to draw the shovels
                for shovel in self.shovels:
                    if 0 < pyxel.frame_count % 20 < 10:
                        pyxel.blt(shovel.x, shovel.y, *shovel.image, colkey=self.black)

                # draw lemming
                for lemming in self.lems:
                    if lemming:
                        pyxel.blt(lemming.x, lemming.y, *lemming.image, colkey=self.black)

                # UI - scoreboard, cursor
                pyxel.line(0, 31, 255, 31, self.white)
                pyxel.blt(self.selection_block.x, self.selection_block.y, *self.selection_block.selection_image,
                          colkey=self.black)
                pyxel.text(2, 1, 'Level: ' + str(self.level), self.white)
                pyxel.text(2, 9, 'Alive: ' + str(self.lems_alive), self.white)
                pyxel.text(2, 17, 'Saved: ' + str(self.lems_saved), self.white)
                pyxel.text(2, 25, 'Died: ' + str(self.lems_dead), self.white)
                pyxel.line(38, 0, 38, 31, self.white)

                pyxel.text(41, 1, 'Ladders: ' + str(self.ladders_number - len(self.ladders)), self.white)
                pyxel.text(41, 9, 'Umbrellas: ' + str(self.umbrella_number - len(self.umbrellas)), self.white)
                pyxel.text(41, 17, 'Blockers: ' + str(self.blockers_number-len(self.blockers)), self.white)
                pyxel.text(41, 25, 'Shovels: ' + str(self.shovels_number-len(self.shovels)), self.white)
                pyxel.line(92, 0, 92, 31, self.white)
                pyxel.text(95, 1, 'Controls', self.white)
                pyxel.line(92, 7, 128, 7, self.white)
                pyxel.line(128, 0, 128, 7, self.white)
                pyxel.text(132, 1, '[SPACE] - Pause/Unpause', self.white)
                # the ladders
                lad = Ladder(95, 16, 'right', True, True)
                pyxel.blt(lad.x, lad.y, *lad.image, colkey=self.black)
                pyxel.text(lad.x - 2, 10, ' [1]', self.white)
                lad2 = Ladder(115, lad.y, 'left', True, True)  # left ladder
                pyxel.blt(lad2.x, lad2.y, *lad2.image, colkey=self.black)
                pyxel.text(lad2.x - 2, 10, ' [2]', self.white)
                # the umbrella
                umb = Umbrella(135, lad.y, True)
                pyxel.blt(umb.x, umb.y, *umb.image, colkey=self.black)
                pyxel.text(umb.x - 2, 10, ' [3]', self.white)
                # the blocker
                bl = Blocker(155, lad.y, True)
                pyxel.blt(bl.x, bl.y, *bl.image, colkey=self.black)
                pyxel.text(bl.x - 2, 10, ' [4]', self.white)
                # the shovel
                shov= Shovel(175,lad.y-1,False)
                pyxel.blt(shov.x,shov.y,*shov.image,colkey=self.black)
                pyxel.text(shov.x+2,10,"[5]",self.white)
                #skip level
                pyxel.text(196,10,"[ENTER]",self.white)
                pyxel.text(202, 17, "skip", self.white)
                pyxel.text(200, 24, "level", self.white)
                # pause/play symbol
                pyxel.line(230, 0, 230, 31, self.white)
                if self.pause and pyxel.frame_count % 30 in range(0, 15):
                    pyxel.rect(246, 8, 4, 16, self.white)
                    pyxel.rect(238, 8, 4, 16, self.white)
                if not self.pause:
                    pyxel.tri(238, 8, 238, 24, 250, 16, self.white)
                if not self.started:
                    pyxel.text(2,34,"Press [SPACE] to start!",self.white)


Board()